package de.fjobilabs.usernamedatabasetools;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import de.fjobilabs.usernamedatabasetools.database.DatabaseException;
import de.fjobilabs.usernamedatabasetools.writer.UsernameWriter;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 21.12.2017 - 16:56:35
 */
public class UsernameDownloaderCommandLineApplication extends CommandLineApplication {
    
    private boolean overrideFiles;
    
    public UsernameDownloaderCommandLineApplication() {
        super("username-downloader [OPTIONS] [DIRECTORY] [SECTIONS]");
    }
    
    @Override
    protected void addOptions(Options options) {
        options.addOption(new Option("a", "all", false, "Dowloads all sections"));
        options.addOption(new Option("o", "override", false, "Overrides existing files"));
    }
    
    @Override
    protected void run(CommandLine commandLine) {
        if (commandLine.getArgList().size() == 0) {
            System.out.println("Missing: [DIRECTORY]");
            printHelp();
            System.exit(1);
            return;
        }
        File directory = new File(commandLine.getArgList().get(0));
        if (directory.exists() && !directory.isDirectory()) {
            getUi().showInfo("DIRECTORY '" + directory + "' is not a directory");
            printHelp();
            System.exit(1);
            return;
        }
        
        boolean downloadAll = commandLine.hasOption("a");
        this.overrideFiles = commandLine.hasOption("o");
        
        if (downloadAll) {
            downloadAllSections(directory);
        } else if (commandLine.getArgList().size() >= 2) {
            String sectionsString = commandLine.getArgList().get(1);
            try {
                int section = Integer.parseInt(sectionsString);
                downloadSection(directory, section);
            } catch (NumberFormatException e) {
                int hyphenIndex = sectionsString.indexOf('-');
                String startSectionString = sectionsString.substring(0, hyphenIndex);
                String endSectionString = sectionsString.substring(hyphenIndex + 1);
                try {
                    int startSection = Integer.parseInt(startSectionString);
                    int endSection = Integer.parseInt(endSectionString);
                    downloadSections(directory, startSection, endSection);
                } catch (NumberFormatException e1) {
                    getUi().showInfo("Invalid SECTIONS: '" + sectionsString + "'");
                    getUi().showException(e1);
                    System.exit(1);
                    return;
                }
            }
        } else {
            getUi().showInfo("Missing: [SECTIONS]");
            printHelp();
            System.exit(1);
            return;
        }
    }
    
    private void downloadSection(File directory, int section) {
        if (!downloadSingleSection(directory, section)) {
            System.exit(1);
            return;
        }
        getUi().showInfo("Finished!");
    }
    
    private void downloadAllSections(File directory) {
        int maxSection;
        try {
            maxSection = getDatabaseHandler().getMaxSectionId();
        } catch (DatabaseException e) {
            getUi().showError("Failed to get max section ID");
            getUi().showException(e);
            System.exit(1);
            return;
        }
        if (!downloadSections(directory, 0, maxSection)) {
            System.exit(1);
            return;
        }
    }
    
    private boolean downloadSections(File directory, int startSection, int endSection) {
        getUi().showInfo("Downloading sections " + startSection + "-" + endSection
                + " to directory '" + directory + "'...");
        
        for (int section=startSection; section<=endSection; section++) {
            downloadSingleSection(directory, section);
        }
        getUi().showInfo("Finished!");
        return true;
    }
    
    private boolean downloadSingleSection(File directory, int section) {
        getUi().showInfo("Downloading section " + section + " to directory '" + directory + "'...");
        
        Collection<String> usernames;
        try {
            usernames = getDatabaseHandler().getUsernames(section);
        } catch (DatabaseException e) {
            getUi().showError("Failed to load usernames for section " + section + "from database");
            getUi().showException(e);
            return false;
        }
        
        if (usernames.isEmpty()) {
            getUi().showInfo("Section " + section + " does not exist");
            return false;
        }
        
        File sectionFile = new File(directory, "section_" + section + ".txt");
        if (sectionFile.exists()) {
            if (!overrideFiles) {
                getUi().showInfo("Section " + section + " already downloaded");
                return false;
            }
            getUi().showInfo("Overriding section file: " + sectionFile.getName());
        } else {
            try {
                sectionFile.getParentFile().mkdirs();
                sectionFile.createNewFile();
            } catch (IOException e) {
                getUi().showError("Failed to create section file: " + sectionFile);
                getUi().showException(e);
                return false;
            }
        }
        
        UsernameWriter writer = null;
        try {
            writer = new UsernameWriter(sectionFile);
            writer.writeUsernames(usernames);
        } catch (IOException e) {
            getUi().showError("Failed to write usernames to file: " + sectionFile);
            getUi().showException(e);
            return false;
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    getUi().showWarning("Faield to close UsernameWriter for file: " + sectionFile);
                    getUi().showException(e);
                }
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        new UsernameDownloaderCommandLineApplication().init(args);
    }
}
