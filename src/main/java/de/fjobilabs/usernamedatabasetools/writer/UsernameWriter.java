package de.fjobilabs.usernamedatabasetools.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Collection;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 21.12.2017 - 21:41:31
 */
public class UsernameWriter {
    
    private RandomAccessFile file;
    private FileChannel fileChannel;
    
    public UsernameWriter(File file) throws FileNotFoundException {
        this.file = new RandomAccessFile(file, "rw");
        this.fileChannel = this.file.getChannel();
    }
    
    public void writeUsernames(Collection<String> usernames) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(20);
        String separator = System.lineSeparator();
        for (String username : usernames) {
            String line = username + separator;
            byteBuffer.clear();
            byteBuffer.put(line.getBytes(Charset.forName("UTF-8")));
            byteBuffer.flip();
            try {
                this.fileChannel.write(byteBuffer);
            } catch (IOException e) {
                throw new IOException("Failed to write username", e);
            }
        }
    }
    
    public void close() throws IOException {
        try {
            this.file.close();
            this.fileChannel.close();
        } catch (IOException e) {
            throw new IOException("Failed to close file and channel", e);
        }
    }
}
